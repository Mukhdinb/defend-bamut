import pygame

# -----------------INIT--------------------
pygame.init()
pygame.display.set_caption("Deffend Bamut")
programIcon = pygame.image.load('icon_wolf.png')
pygame.display.set_icon(programIcon)
screen = pygame.display.set_mode((1200, 600), pygame.FULLSCREEN, 32)

bg = pygame.image.load('rawData/2d_backgrounds/PNG/game_background_1/game_background_1.png')
sf_width, sf_height = screen.get_size()
bg = pygame.transform.scale(bg, (sf_width, int(sf_height)))
clock = pygame.time.Clock()

bullet_sound = pygame.mixer.Sound('sound/Gun.wav')
hit_sound = pygame.mixer.Sound('sound/mixkit-falling-on-undergrowth-390.wav')

pygame.mixer.music.load('sound/Ambient-Electronic-background-music.wav')
pygame.mixer.music.play(-1)


# ----------------PLAYER------------------
class Player(object):
    player_jump_animation_right = [pygame.image.load('player_mask/jump/test/3_terrorist_3_Jump_000.png'),
                                   pygame.image.load('player_mask/jump/test/3_terrorist_3_Jump_001.png'),
                                   pygame.image.load('player_mask/jump/test/3_terrorist_3_Jump_002.png'),
                                   pygame.image.load('player_mask/jump/test/3_terrorist_3_Jump_003.png'),
                                   pygame.image.load('player_mask/jump/test/3_terrorist_3_Jump_004.png'),
                                   pygame.image.load('player_mask/jump/test/3_terrorist_3_Jump_005.png'),
                                   pygame.image.load('player_mask/jump/test/3_terrorist_3_Jump_006.png'),
                                   pygame.image.load('player_mask/jump/test/3_terrorist_3_Jump_007.png')]

    player_idle_animation = [pygame.image.load('player_mask/idle/3_terrorist_3_Idle_000.png'),
                             pygame.image.load('player_mask/idle/3_terrorist_3_Idle_001.png'),
                             pygame.image.load('player_mask/idle/3_terrorist_3_Idle_002.png'),
                             pygame.image.load('player_mask/idle/3_terrorist_3_Idle_003.png'),
                             pygame.image.load('player_mask/idle/3_terrorist_3_Idle_004.png'),
                             pygame.image.load('player_mask/idle/3_terrorist_3_Idle_005.png'),
                             pygame.image.load('player_mask/idle/3_terrorist_3_Idle_006.png'),
                             pygame.image.load('player_mask/idle/3_terrorist_3_Idle_007.png')]

    player_walk_right_animation = [pygame.image.load('player_mask/walk/3_terrorist_3_Walk_000.png'),
                                   pygame.image.load('player_mask/walk/3_terrorist_3_Walk_001.png'),
                                   pygame.image.load('player_mask/walk/3_terrorist_3_Walk_002.png'),
                                   pygame.image.load('player_mask/walk/3_terrorist_3_Walk_003.png'),
                                   pygame.image.load('player_mask/walk/3_terrorist_3_Walk_004.png'),
                                   pygame.image.load('player_mask/walk/3_terrorist_3_Walk_005.png'),
                                   pygame.image.load('player_mask/walk/3_terrorist_3_Walk_006.png'),
                                   pygame.image.load('player_mask/walk/3_terrorist_3_Walk_007.png')]

    player_walk_left_animation = []
    player_idle_animation_inv = []
    player_jump_animation_left = []

    def __init__(self, x, y, width, height):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.velocity = 5
        self.jump = False
        self.jump_count = 10
        self.left = False
        self.right = True
        self.walkCount = 0
        self.standing = True
        self.hitbox = (self.x, self.y, self.width, self.height)
        self.score = 0

        for x in range(8):
            self.player_jump_animation_right[x] = pygame.transform.scale(self.player_jump_animation_right[x],
                                                                         (width, height))
            self.player_idle_animation[x] = pygame.transform.scale(self.player_idle_animation[x],
                                                                   (width, height))
            self.player_walk_right_animation[x] = pygame.transform.scale(self.player_walk_right_animation[x],
                                                                         (width, height))

            self.player_walk_left_animation.append(pygame.transform.flip(self.player_walk_right_animation[x],
                                                                         True, False))
            self.player_idle_animation_inv.append(pygame.transform.flip(self.player_idle_animation[x],
                                                                        True, False))
            self.player_jump_animation_left.append(pygame.transform.flip(self.player_jump_animation_right[x],
                                                                         True, False))

    def draw(self, win):
        if self.walkCount + 1 >= 24:
            self.walkCount = 0

        if self.jump:
            if self.left:
                win.blit(self.player_jump_animation_left[int(self.walkCount / 3)], (self.x, self.y))
                self.walkCount += 1
            elif self.right:
                win.blit(self.player_jump_animation_right[int(self.walkCount / 3)], (self.x, self.y))
                self.walkCount += 1
        else:
            if not self.standing:
                if self.left:
                    win.blit(self.player_walk_left_animation[int(self.walkCount / 3)], (self.x, self.y))
                    self.walkCount += 1
                elif self.right:
                    win.blit(self.player_walk_right_animation[int(self.walkCount / 3)], (self.x, self.y))
                    self.walkCount += 1
            else:
                if self.left:
                    win.blit(self.player_idle_animation_inv[int(self.walkCount / 3)], (self.x, self.y))
                    self.walkCount += 1
                if self.right:
                    win.blit(self.player_idle_animation[int(self.walkCount / 3)], (self.x, self.y))
                    self.walkCount += 1
        self.hitbox = (self.x, self.y, self.width, self.height)
        pygame.draw.rect(win, (255, 0, 0),
                         (self.x, self.y - (int(self.height / 10) * 2), self.width, int(self.height / 10)))
        pygame.draw.rect(win, (0, 255, 0),
                         (self.x, self.y - (int(self.height / 10) * 2), self.width, int(self.height / 10)))
        # pygame.draw.rect(win, (255, 0, 0), self.hitbox, 2)

    def hit(self):
        self.score -= 10


# -----------------Projectiler--------

class Projectile(object):
    def __init__(self, x, y, radius, color, facing, width, height):
        self.x = x
        self.y = y
        self.radius = radius
        self.color = color
        self.facing = facing
        self.vel = 8 * facing
        self.bullet = pygame.image.load('rawData/2d_terrorists/scml/terrorist_1/shot.png')
        self.bullet = pygame.transform.scale(self.bullet, (width, height))

    def draw(self, win):
        win.blit(self.bullet, (self.x, self.y))
        # pygame.draw.circle(win, self.color, (self.x, self.y), self.radius)


# ---------------ENEMY------------------
class Enemy(object):
    walk_right_animation = [pygame.image.load('rawData/2d_police/png/1/walk/1_police_Walk_000.png'),
                            pygame.image.load('rawData/2d_police/png/1/walk/1_police_Walk_001.png'),
                            pygame.image.load('rawData/2d_police/png/1/walk/1_police_Walk_002.png'),
                            pygame.image.load('rawData/2d_police/png/1/walk/1_police_Walk_003.png'),
                            pygame.image.load('rawData/2d_police/png/1/walk/1_police_Walk_004.png'),
                            pygame.image.load('rawData/2d_police/png/1/walk/1_police_Walk_005.png'),
                            pygame.image.load('rawData/2d_police/png/1/walk/1_police_Walk_006.png'),
                            pygame.image.load('rawData/2d_police/png/1/walk/1_police_Walk_007.png')]

    idle_right_animation = [pygame.image.load('rawData/2d_police/png/1/idle/1_police_Idle_000.png'),
                            pygame.image.load('rawData/2d_police/png/1/idle/1_police_Idle_001.png'),
                            pygame.image.load('rawData/2d_police/png/1/idle/1_police_Idle_002.png'),
                            pygame.image.load('rawData/2d_police/png/1/idle/1_police_Idle_003.png'),
                            pygame.image.load('rawData/2d_police/png/1/idle/1_police_Idle_004.png'),
                            pygame.image.load('rawData/2d_police/png/1/idle/1_police_Idle_005.png'),
                            pygame.image.load('rawData/2d_police/png/1/idle/1_police_Idle_006.png'),
                            pygame.image.load('rawData/2d_police/png/1/idle/1_police_Idle_007.png')]

    jump_right_animation = [pygame.image.load('rawData/2d_police/png/1/jump/1_police_Jump_000.png'),
                            pygame.image.load('rawData/2d_police/png/1/jump/1_police_Jump_001.png'),
                            pygame.image.load('rawData/2d_police/png/1/jump/1_police_Jump_002.png'),
                            pygame.image.load('rawData/2d_police/png/1/jump/1_police_Jump_003.png'),
                            pygame.image.load('rawData/2d_police/png/1/jump/1_police_Jump_004.png'),
                            pygame.image.load('rawData/2d_police/png/1/jump/1_police_Jump_005.png'),
                            pygame.image.load('rawData/2d_police/png/1/jump/1_police_Jump_006.png'),
                            pygame.image.load('rawData/2d_police/png/1/jump/1_police_Jump_007.png')]

    dead_right_animation = [pygame.image.load('rawData/2d_police/png/1/hurt/1_police_Hurt_000.png'),
                            pygame.image.load('rawData/2d_police/png/1/hurt/1_police_Hurt_001.png'),
                            pygame.image.load('rawData/2d_police/png/1/hurt/1_police_Hurt_002.png'),
                            pygame.image.load('rawData/2d_police/png/1/hurt/1_police_Hurt_003.png'),
                            pygame.image.load('rawData/2d_police/png/1/hurt/1_police_Hurt_004.png'),
                            pygame.image.load('rawData/2d_police/png/1/hurt/1_police_Hurt_005.png'),
                            pygame.image.load('rawData/2d_police/png/1/hurt/1_police_Hurt_006.png'),
                            pygame.image.load('rawData/2d_police/png/1/hurt/1_police_Hurt_007.png'),
                            pygame.image.load('rawData/2d_police/png/1/hurt/1_police_Hurt_008.png')]

    walk_left_animation = []
    idle_left_animation = []
    jump_left_animation = []
    dead_left_animation = []

    def __init__(self, x, y, width, height, end):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.end = end
        self.walk_count = 0
        self.vel = 3
        self.path = [self.x, self.end]
        self.hitbox = (self.x, self.y, self.width, self.height)
        self.health = 100
        self.visible = True
        self.dead = False
        self.dead_count = 0

        for x in range(8):
            self.jump_right_animation[x] = pygame.transform.scale(self.jump_right_animation[x],
                                                                  (width, height))
            self.walk_right_animation[x] = pygame.transform.scale(self.walk_right_animation[x],
                                                                  (width, height))
            self.idle_right_animation[x] = pygame.transform.scale(self.idle_right_animation[x],
                                                                  (width, height))
            self.dead_right_animation[x] = pygame.transform.scale(self.dead_right_animation[x],
                                                                  (width * 2, height))

            self.jump_left_animation.append(pygame.transform.flip(self.jump_right_animation[x],
                                                                  True, False))
            self.walk_left_animation.append(pygame.transform.flip(self.walk_right_animation[x],
                                                                  True, False))
            self.idle_left_animation.append(pygame.transform.flip(self.idle_right_animation[x],
                                                                  True, False))
            self.dead_left_animation.append(pygame.transform.flip(self.dead_right_animation[x],
                                                                  True, False))
        self.dead_right_animation[8] = pygame.transform.scale(self.dead_right_animation[8],
                                                              (width * 2, height))
        self.dead_left_animation.append(pygame.transform.flip(self.dead_right_animation[8],
                                                              True, False))

    def draw(self, win):

        if self.walk_count + 1 > 24:
            self.walk_count = 0

        if self.dead:
            if self.dead_count < 24:
                self.dead_count += 1
            if self.vel > 0:  ### why does corpse disaper at picture 8?
                print(int(self.dead_count / 3))
                win.blit(self.dead_right_animation[int(self.dead_count / 3)], (self.x, self.y))
                # self.dead_count += 1
            else:
                print(int(self.dead_count / 3))
                win.blit(self.dead_left_animation[int(self.dead_count / 3)], (self.x, self.y))
                # self.dead_count += 1

        else:
            self.move()
            if self.vel > 0:
                win.blit(self.walk_right_animation[int(self.walk_count / 3)], (self.x, self.y))
                self.walk_count += 1
            else:
                win.blit(self.walk_left_animation[int(self.walk_count / 3)], (self.x, self.y))
                self.walk_count += 1

        self.hitbox = (self.x, self.y, self.width, self.height)
        pygame.draw.rect(win, (255, 0, 0),
                         (self.x, self.y - (int(self.height / 10) * 2), self.width, int(self.height / 10)))
        pygame.draw.rect(win, (0, 255, 0),
                         (self.x, self.y - (int(self.height / 10) * 2), int(self.width * self.health / 100),
                          int(self.height / 10)))

        # pygame.draw.rect(win, (255, 0, 0), self.hitbox, 2)

    def move(self):
        if self.vel > 0:
            if (self.x + self.vel) < self.path[1]:
                self.x += self.vel
            else:
                self.vel = self.vel * (-1)
                self.walk_count = 0
        else:
            if (self.x - self.vel) > self.path[0]:
                self.x += self.vel
            else:
                self.vel = self.vel * (-1)
                self.walk_count = 0

    def hit(self):
        if self.health >= 1:
            self.health -= 10
        else:
            self.dead = True
            print("enemy hit")


# ---------------Functions--------------
def redraw_game_window():
    screen.blit(bg, (0, 0))
    score_text = score_font.render('Score: ' + str(player.score), True, (0, 0, 0))
    screen.blit(score_text, (int(sf_width * 0.85), int(sf_height * 0.05)))
    player.draw(screen)
    enemy.draw(screen)
    for bullet in bullets:
        bullet.draw(screen)

    pygame.display.update()


# ---------------Main------------------
unit_width = int(sf_width / 25)
unit_height = int(sf_height / 7)
bullet_width = int(sf_width * 0.012)
bullet_height = int(sf_height * 0.0071)
unit_land_pos = int(sf_height * 0.8)
player_start_pos = int(sf_width * 0.2)
enemy_start_pos = int(sf_width * 0.6)
score_font = pygame.font.SysFont('comicsans', 30, True, True)

player = Player(player_start_pos, unit_land_pos, unit_width, unit_height)
enemy = Enemy(enemy_start_pos, unit_land_pos, unit_width, unit_height, enemy_start_pos + 100)
#enemy2 = Enemy(enemy_start_pos + 100, unit_land_pos, unit_width, unit_height, enemy_start_pos + 100)

bullets = []
shoot_loop = 0
running = True
while running:
    clock.tick(160)

    if not enemy.dead:
        if (player.hitbox[1] < enemy.hitbox[1] + enemy.hitbox[3]) and (
                player.hitbox[1] + player.hitbox[3] > enemy.hitbox[1]):
            if (player.hitbox[0] + player.hitbox[2] > enemy.hitbox[0]) and (
                    player.hitbox[0] < enemy.hitbox[0] + enemy.hitbox[2]):
                player.hit()
    if shoot_loop > 0:
        shoot_loop += 1
    if shoot_loop > 5:
        shoot_loop = 0
    screen.fill((0, 255, 0))
    # screen.blit(background, (0, 0))
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    for bullet in bullets:
        if (bullet.y - bullet_height < enemy.hitbox[1] + enemy.hitbox[3]) and (
                bullet.y + bullet_height > enemy.hitbox[1]):
            if (bullet.x - bullet_width > enemy.hitbox[0]) and (
                    bullet.x - bullet_width < enemy.hitbox[0] + enemy.hitbox[2]):
                hit_sound.play()
                enemy.hit()
                player.score += 1
                bullets.pop(bullets.index(bullet))

        if (bullet.x < sf_width) and (bullet.x > 0):
            bullet.x += bullet.vel
        else:
            bullets.pop(bullets.index(bullet))
    keys = pygame.key.get_pressed()
    if keys[pygame.K_x]:
        running = False

    if keys[pygame.K_SPACE] and shoot_loop == 0:
        bullet_sound.play()
        if player.left:
            direction = -1
        else:
            direction = 1

        if len(bullets) < 10:
            bullets.append(Projectile(round(player.x + player.width // 2),
                                      round(player.y + player.height // 2),
                                      6, (0, 0, 0), direction, bullet_width, bullet_height))
        shoot_loop = 1
    if keys[pygame.K_a] and player.x > 5:
        player.x -= player.velocity
        player.left = True
        player.right = False
        player.standing = False
    elif keys[pygame.K_d] and player.x < sf_width - 50:
        player.x += player.velocity
        player.left = False
        player.right = True
        player.standing = False
    else:
        player.standing = True

    if not player.jump:
        # move up and down
        # if keys[pygame.K_w] and playerY > 5:
        #    playerY -= player_velocity
        # if keys[pygame.K_s] and playerY < 550:
        #    playerY += player_velocity
        if keys[pygame.K_w]:
            player.jump = True
            player.walkCount = 0
    else:
        if player.jump_count >= -10:
            player.y -= (player.jump_count * abs(player.jump_count)) * 0.20
            player.jump_count -= 1
        else:
            player.jump_count = 10
            player.jump = False

    redraw_game_window()

pygame.quit()
